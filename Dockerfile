FROM centos:6

# install packages
RUN yum -y update
RUN yum -y install httpd php curl mod_perl
RUN yum -y install perl-devel perl-CPAN
RUN yum -y install make gcc expat-devel

# install cpanminus
RUN curl -L http://cpanmin.us | perl - App::cpanminus

# install required modules
RUN cpanm Digest::MD5 MD5 HTML::Tagset HTML::Parser Math::Random PDF \
    Storable URI Bundle::libnet XML::Simple BSD::Resource Apache::Session \
    Crypt::Tea Tie::IxHash XML::Writer Spreadsheet::SimpleExcel
# has issues sometimes
RUN cpanm XML::Hash::LX PDF::Create 
    # missing
    # libwww Mime ascii2pdf
RUN cpanm Apache2::Connection

RUN yum -y install epel-release
RUN yum -y install mod_python perl-CGI perl-ExtUtils-Embed httpd-devel
RUN cpanm ModPerl::MM
RUN cpanm Apache2::Reload

# add start script
COPY httpd-foreground /usr/local/bin/

EXPOSE 80
CMD ["httpd-foreground"]
